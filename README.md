# [Pinephone Pro] Singularity

Singularity for the PinePhone Pro Explorer Edition

## Getting started

Install `git` and `manjaro-arm-tools`.

```bash
sudo pacman -S git manjaro-arm-tools
```

Download Manjaro's profiles.

```bash
sudo getarmprofiles -f
```

Update the profiles at `/usr/share/manjaro-arm-tools/profiles` using git.

```bash
cd /usr/share/manjaro-arm-tools/profiles/arm-profiles && \
sudo git remote set-url origin https://gitlab.com/waser-technologies/profiles/arm/arm-profiles.git && \
sudo git pull --rebase origin && \
sudo git fetch
```

Build an ISO image.

```bash
sudo buildarmimg -d pinephonepro -e singularity -v dev-20220306 -b unstable -s Singularity -k https://wasertech.github.io/singularity/aarch64/singularity.db
```

Your images are located at `/var/cache/manjaro-arm-tools/img/`